var express = require("express");
var app = express();
var cfenv = require("cfenv");
var bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

var ibmdb = require('ibm_db');
var vcapLocal;
try {
  vcapLocal = require('./vcap-local.json');
  console.log("Loaded local VCAP", vcapLocal);
} catch (e) { }

const appEnvOpts = vcapLocal ? { vcap: vcapLocal} : {}
const appEnv = cfenv.getAppEnv(appEnvOpts);


// we need to build up this connection string some information passed to us from environment variables
// we get this from the environment variable VCS_SERVICES which you can see using the following:
//    ibmcloud cf venv <app name>
var db2 = appEnv.services["dashDB For Transactions"][0]["credentials"]["connection"]["db2"]
var dbname=db2.database
var hostname=db2["hosts"][0].hostname
var dbport=db2["hosts"][0].port
var protocol='TCPIP'
var uid=db2["authentication"]["username"]
var password=db2["authentication"]["password"]
var dsn = `DATABASE=${dbname};HOSTNAME=${hostname};PORT=${dbport};PROTOCOL=${protocol};UID=${uid};PWD=${password};Security=SSL`;


//serve static file (index.html, images, css)
app.use(express.static(__dirname + '/views'));

app.get("/hello", function(request, response){
  response.send({ "hello": "world" });
})

// do not do this in real life - but it shows you the environment variables
app.get("/venv", function(request, response){
  response.send({ venv: appEnv })
})

app.get("/api/create-table", function (request, response) {
  try {
    let conn = ibmdb.openSync(dsn)
    let tabledef="create table events "+
                  "(eid int not null generated always as identity (start with 1000, increment by 1),"+
                    "shortname varchar(20) not null,"+
                    "location varchar(60) not null,"+
                    "begindate timestamp not null,"+
                    "enddate timestamp not null,"+
                    "contact varchar(255) not null);";
    let data=conn.querySync(tabledef);
    conn.closeSync();
    response.send({result : data});
  } catch( e) {
    response.send( { dberror: e})
  }
});

app.get("/api/insert-into-table", function (request, response) {
  try {
    let conn = ibmdb.openSync(dsn)
    let sampledata="insert into events(shortname,location,begindate,enddate,contact) values('Think 2019','San Francisco','2019-02-12 00:00:00','2019-02-15 23:59:00','https://www.ibm.com/events/think/'),('IDUG2019','Charlotte','2019-06-02 00:00:00','2019-06-06 23:59:00','http://www.idug.org');"
    let data=conn.querySync(sampledata);
    conn.closeSync();
    response.send( {result : data});
  } catch( e) {
    response.send( { dberror: e})
  }
});

app.get("/api/select-from-table", function (request, response) {
  try {
    let conn = ibmdb.openSync(dsn)
    let sampledata="select * from events;"
    let data=conn.querySync(sampledata);
    conn.closeSync();
    response.send( {result : data});
  } catch( e) {
    response.send( { dberror: e})
  }
});

app.get("/api/drop-table", function (request, response) {
  try {
    let conn = ibmdb.openSync(dsn)
    let tabledrop="drop table events;"
    let data=conn.querySync(tabledrop);
    conn.closeSync();
    response.send( {result : data});
  } catch( e) {
    response.send( { dberror: e})
  }
});


var port = process.env.PORT || 3000
app.listen(port, function() {
    console.log("To view your app, open this link in your browser: http://localhost:" + port);
});
