# DB2 Example

This example has been built off of the get-started-node project suppplied in the IBM Cloud Foundry service.  The manifest file was modified to point to the existing Cloud Foundry application I had created:

manifest.yml

```
---
applications:
 - name: <your cloud foundry service name>
   host: <the start of the cloud foundry service app url>
   memory: 256M
```

The tricky bit is working out what all the VCAP_SERVICES piece is all about.  You can learning more about these here [Cloud Foundry Documentation](https://docs.cloudfoundry.org/devguide/deploy-apps/environment-variable.html).

To view the VCAP_SERVICES in full as the website does only allows you to see the top:

```
ibmcloud cf venv <name of cloud foundry service>
```

It is also worth knowing that the DB2 connection type is called dashDB-for-transactions.

## How to recreate

1. Create a DB2 on Cloud instance
2. Create a new Cloud Foundry application, in this case node.js
3. Add a connection to the Cloud Foundry Application you created

